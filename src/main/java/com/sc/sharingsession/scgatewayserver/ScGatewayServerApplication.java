package com.sc.sharingsession.scgatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScGatewayServerApplication.class, args);
	}

}
